#include <stdio.h>

int main(void)
{
	for(int i=1; i<=100; i++){
		if(i%2==1){
			printf("%d\n",i);
		}
	}

	for(int i=1; i<=100; i++){
		if(i%2==0){
			printf("%d\n",i);
		}
	}

	for(int i=2; i<=100; i++){
		int count = 0;
		for(int j=1; j<=i; j++){
			if(i%j==0){
				count++;
			}
		}
		if(count==2){
			printf("%d\n",i);
		}
	}
	//To test deleting branch
	return 0;
}
	
